import * as React from 'react';
import {Button, Image, ScrollView, Text, StyleSheet, View} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import * as firebase from 'firebase';
import Spinner from 'react-native-loading-spinner-overlay';

// TODO: update the constant to your own value.
const CLOUD_FUNCTION_HOST = `https://us-central1-ocr-example-281705.cloudfunctions.net`;
const IMAGE_BUCKET = 'ocr-uploaded'
const firebaseConfig = {
    apiKey: "AIzaSyBTjAo_MbtlSuli6dfGljjOP4VuutoXYUo",
    authDomain: "ocr-example-281705.firebaseapp.com",
    databaseURL: "https://ocr-example-281705.firebaseio.com",
    projectId: "ocr-example-281705",
    storageBucket: "ocr-example-281705.appspot.com",
    messagingSenderId: "510288558856",
    appId: "1:510288558856:web:b06e6547398244539d8abd"
};

const styles = StyleSheet.create({
    container: {
        height: '50%'
    },
    myElement: {
        padding: 10
    },
    spinnerTextStyle: {
        color: '#FFF'
    },
    scrollView: {
        backgroundColor: "#eaeaea"
    },

});
const app = firebase.initializeApp(firebaseConfig);
const storage = app.storage(`gs://${IMAGE_BUCKET}`);
const SUBFOLDER = '/images';


const ref = storage.ref().child(SUBFOLDER);

export default class HomePage extends React.Component {
    state = {
        image: null,
        ocrResult: null,
        spinner: false,
        spinnerText: ""
    };

    render() {
        let {image, text, ocrResult} = this.state;

        return (
            <>
                <View style={{paddingTop: Constants.statusBarHeight}}/>
                <View style={styles.container}>
                    <Spinner
                        visible={this.state.spinner}
                        textContent={this.state.spinnerText}
                        textStyle={styles.spinnerTextStyle}
                    />
                    <Button style={styles.myElement} title="Take an image from camera" onPress={this._pickImage}/>
                    {image && <Image source={{uri: image}} style={{width: 200, height: 200, padding: 10, margin: 20}}/>}
                    {image && <Button style={styles.myElement} title="Detect Text" onPress={this._ocr}/>}

                </View>
                <View
                    style={{
                        borderBottomColor: 'black',
                        borderBottomWidth: 1,
                    }}
                />
                <ScrollView style={[styles.container, styles.scrollView]}>
                    {ocrResult &&
                    <Text style={styles.text}>OCR Result:{"\n"}{ocrResult}</Text>}
                </ScrollView>
            </>

        );
    }

    componentDidMount() {
        this.getPermissionAsync();
    }

    getPermissionAsync = async () => {
        if (Constants.platform.ios) {
            const {status} = await Permissions.askAsync(Permissions.CAMERA);
            if (status !== 'granted') {
                alert("Sorry, we need camera roll permissions to make this work!");
            }
        }
    };
    _ocr = async () => {
        // const image = {
        //     uri: this.state.image,
        //     type: 'image/jpeg',
        //     name: 'myImage' + '-' + Date.now() + '.jpg'
        // }
        this.setState({spinner: true, spinnerText: "Uploading Image..."});
        const filename = 'myImage' + '-' + Date.now() + '.jpg';
        const response = await fetch(this.state.image);

        // TODO: zip the image?
        const blob = await response.blob();
        ref.child(filename).put(blob, {
            name: filename
        }).then((snapshot) => {
            console.log("Uploaded a blob or file!", snapshot);
            this.setState({spinner: true, spinnerText: "Detecting text..."});
            const url = `${CLOUD_FUNCTION_HOST}/processImage?name=${filename}`;
            // Perform the request. Note the content type - very important
            fetch(url, {
                method: 'GET'
            }).then(data => data.json()).then(data => {
                // Just me assigning the image url to be seen in the view
                console.log("ocr results", data);
                this.setState({ocrResult: data['data'], spinner: false, spinnerText: ""});
            }).catch(error => {
                this.setState({spinner: false, spinnerText: ""});
                console.error("Failed to get OCR result", error);
                alert("Failed to get OCR result: " + error?.message)
            });

        }).catch(err => {
            this.setState({spinner: false, spinnerText: ""});
            console.log("firebase put error", err);
            alert("Failed to upload images: " + err?.message)
        });
        // Instantiate a FormData() object
        // append the image to the object with the title 'image'

    }
    _pickImage = async () => {
        try {
            await ImagePicker.getCameraPermissionsAsync();
            let result = await ImagePicker.launchCameraAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                quality: 1,
            });
            if (!result.cancelled) {
                this.setState({image: result.uri});
            }
            console.log(result);
        } catch (E) {
            console.log("Cannot take picture", E);
            alert("Cannot take picture: " + E);
        }
    };
}
