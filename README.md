# ocr-test

An OCR app using the Expo, React Native & Google Cloud Platform

Run `expo start` to run it on devices.

If you want to use your own GCP & Firebase service, please update the Cloud Function host & Firebase config in `HomePage.jsx`

Data saved in Google Cloud Storage.